package edu.uoc.uocnejitos.model;

abstract interface  Movable {
	public boolean isValidMove(Coordinate destination, Level level);
	
	public boolean move(Coordinate destination, Level level);
		
	

}
