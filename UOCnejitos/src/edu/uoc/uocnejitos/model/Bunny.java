package edu.uoc.uocnejitos.model;

public class Bunny extends Piece implements Movable {
	public Bunny (Coordinate coord) {
		super(coord, Symbol.BUNNY_BROWN);
	}
	public Bunny (Coordinate coord,Symbol symbol) {
		super(coord,symbol);
		
	}
	
	public boolean isInHole() {
		
		return (this.getSymbol() == Symbol.BUNNY_BROWN_HOLE ||
				this.getSymbol() == Symbol.BUNNY_GRAY_HOLE  ||
				this.getSymbol() == Symbol.BUNNY_WHITE_HOLE);
	}
	
	@Override
	/*
	 
	 */
	public boolean isValidMove(Coordinate destination, Level level) {
		//return	this.move(destination,level);
		if(this.getCoord().equals(destination)) {
			return ;
		}
	}

	@Override
	public boolean move(Coordinate destination, Level level) {
		if(destination == null || level == null || this.getCoord() == null ) {//getcoord o getSymbol?
			return false;
		}
		if(isValidMove(destination,level)) {
			
		try {
			if(	level.getPiece​(destination).getSymbol() == Symbol.HOLE) {
				
				if(this.getSymbol() == Symbol.BUNNY_BROWN) this.setSymbol​(Symbol.BUNNY_BROWN_HOLE);
				if(this.getSymbol() == Symbol.BUNNY_GRAY)  this.setSymbol​(Symbol.BUNNY_GRAY_HOLE);
				if(this.getSymbol() == Symbol.BUNNY_WHITE) this.setSymbol​(Symbol.BUNNY_WHITE_HOLE);
			}
				try {
						level.setPiece​(destination, this);
						return true;
				} catch (LevelException e1) {
						e1.printStackTrace();
				}
		} catch (LevelException e2) { 
				e2.printStackTrace();
		}
			
		}
		return false;
	}
	
	
	
	private boolean isValidHorizontalMove​ (Move move,Level level) {
		if(move.getDirection() == MoveDirection.HORIZONTAL && 
		   level.isObstacle​(this.getCoord())) {
			return true;
		}
		
		
		return false;
	}
	
	private boolean isValidVerticalMove(Move move,Level level) {
		
		if(move.getDirection() == MoveDirection.VERTICAL && 
				   level.isObstacle​(this.getCoord())) {
					return true;
				}
		return false;
	}
	
	
	
	
	
	
}
