package edu.uoc.uocnejitos.model;

public class Coordinate implements Comparable<Coordinate> {
	private int row;
	private int column;
	
	public Coordinate(int row, int column) {
		setRow​(row);
		setColumn​(column);
	}
	
	public int getRow() {
		return row;
	
	}
	public void setRow​(int row) {
		this.row = row;
	}
	public int getColumn() {
		return column; 
	}
	public void setColumn​(int column) {
		this.column = column;
		
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}

		Coordinate coor1 = (Coordinate) obj ; //Creamos nuevo objeto coor1. Almacenaremos lo que ha en obj haciendo cast.
		if(this.getRow()  == (coor1.row) && 
		   this.getColumn() == (coor1.column)) {
			return true;
			}
		return false;
	}

	@Override
	public String toString() {
		 
		return "("+this.getRow()+","+this.getColumn()+")";
	}

	@Override
	public int compareTo(Coordinate other) {
		
		if (this.equals(other)) return 0;
		if (this.getRow() <= other.row) {
			if(	this.getColumn() <= other.column) {
			return -1;
			}
		}
		return 1 ;
	}
	
	
}