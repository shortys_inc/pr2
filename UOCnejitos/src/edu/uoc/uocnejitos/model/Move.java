package edu.uoc.uocnejitos.model;

public class Move {
	
		private Coordinate coordStart;
		private Coordinate coordEnd;
		
		
		 public Move(int xStart,int yStart,int xEnd,int yEnd) {
			Coordinate coord1 = new Coordinate(xStart,yStart); 
			Coordinate coord2 = new Coordinate(xEnd,yEnd); 
			coordStart = coord1;
			coordEnd = coord2;
			setStart​(coordStart);
			setEnd​(coordEnd);

		}
		public Move(Coordinate coordStart,Coordinate coordEnd) {
			setStart​(coordStart);
			setEnd​(coordEnd);
		}	
		public Coordinate getStart() {
			return this.coordStart;
		}
		public void setStart​(Coordinate start) {
			coordStart = start;
		}
		public int getRowStart() {
			return coordStart.getRow();
		}
		public void setRowStart​(int rowStart) {
			coordStart.setRow​(rowStart);
		}
		public int getColumnStart() {
			return coordStart.getColumn();
		}
		public void setColumnStart​(int columnStart) {
			coordStart.setColumn​(columnStart);
		}
		public Coordinate getEnd() {
			return coordEnd;
		}
		public void setEnd​(Coordinate end) {
			coordEnd = end;
		}
		public int getRowEnd() {
			return coordEnd.getRow();
		}
		public void setRowEnd​(int rowEnd) {
			coordEnd.setRow​(rowEnd);
		}
		public int getColumnEnd() {
			return coordEnd.getColumn();
		}
		public void setColumnEnd​(int columnEnd) {
			 coordEnd.setColumn​(columnEnd);
		}
		public MoveDirection getDirection() {
			if(this.coordEnd.getRow() == this.coordStart.getRow()) {
				if(this.coordEnd.getColumn() != this.coordStart.getColumn()) {
					return MoveDirection.HORIZONTAL;
				}
			}
			if(this.coordEnd.getColumn() == this.coordStart.getColumn()) {
				if(this.coordEnd.getRow() != this.coordStart.getRow()) {
					return MoveDirection.VERTICAL;
				}
			}
			return MoveDirection.INVALID;
		}
		public int getHorizontalDistance() {
			return coordEnd.getColumn() - coordStart.getColumn();
		}
		public int getVerticalDistance() {
			return coordEnd.getRow()-coordStart.getRow();
		}
		@Override 
		public String toString() {
			return "("+getRowStart()+","+getColumnStart()+") --> ("+ getRowEnd()+","+getColumnEnd()+") : "+getDirection();
			}
	}


