package edu.uoc.uocnejitos.model;

public abstract class Piece {
	private Coordinate 	coord;
	private Symbol 	symbol;
	protected Piece(Coordinate coord, Symbol symbol){
		this.coord = coord;
		setSymbol​(symbol);
	}
	public Coordinate getCoord() {
		return coord;
	}
	public Symbol getSymbol() {
		return symbol;
	}
	public void setCoord(Coordinate coord) {
		
		this.coord.setRow​(coord.getRow());
		this.coord.setColumn​(coord.getColumn());

	}
	public void setCoord​(int row,int column) {
		this.coord.setRow​(row);
		this.coord.setColumn​(column);
	}
	public void setSymbol​(Symbol symbol) {
		this.symbol = symbol;
	}
	@Override
	public String toString() {
		 return String.valueOf(symbol);
	}
	
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Piece piece1 = (Piece) obj ; 
		 if(this.getSymbol().equals(piece1.symbol)) {
			 if(this.getCoord().equals(piece1.coord)) {
				 return true;
			 }
		 }
			return false;
	}
}
