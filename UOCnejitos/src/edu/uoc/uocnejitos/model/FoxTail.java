package edu.uoc.uocnejitos.model;

public class FoxTail extends Fox {

	protected FoxTail(FoxHead foxHead) {
		super(foxHead.getCoord(), foxHead.getSymbol(), foxHead.getDirection());
		/*constructor debe inicializar la coordenada de la cola en función de la
		coordenada de la cabeza que recibe como parámetros.*/
		calculateCoord​(foxHead.getCoord());
	}

	public Coordinate calculateCoord​(Coordinate coordHead) {
		return coordHead;	
		//this.setOtherHalf​(this);
	}
	
	private Coordinate getHeadEndCoordinate​(Coordinate tailDestination,	int sizeBoard) {
		return tailDestination;
	}
	
	@Override
	public boolean isValidMove(Coordinate destination, Level level) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean move(Coordinate destination, Level level) {
		// TODO Auto-generated method stub
		return false;
	}

}
